package dam.ismael;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "LOG-";

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onStart " );
        notify(" onStart ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onStop ");
        notify(" onStop ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onPause ");
        notify(" onPause ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onResume ");
        notify(" onResume ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestart ");
        notify(" onRestart ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //true finaliza el usuario / false finalizxa el sistema.
        Log.i(DEBUG_TAG+ getLocalClassName(), " onDestry " + textoisFishing());
        notify(" onDestry ");
    }

    //se llama cuando se destruye la aplicacion, como cunado giras la pantalla
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestore ");
        notify(" onRestore ");
    }
    //sirve para crear un save de la aplicacion.
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onSave ");
        notify(" onSave ");
    }

    private void notify(String eventName){
        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,"My Lifecycle",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName+" "+activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int)System.currentTimeMillis(), notificationBuilder.build());
    }

    private String textoisFishing(){
        if(isFinishing()){
            return "El usuario a cerrado la aplicacion.";
        }else{
            return "El sistema a cerrado la aplicacion.";
        }
    }

}
